using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlJugador : MonoBehaviour
{
    public float rapidezDesplazamiento = 4;
    public int Health; // HAY QUE REFERENCIAR AL HUD
    public Camera camara;
    public GameObject fuego;
    private GameManager Manager;
    private UI_Manager HUD;
   
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Manager = GameManager.instance;
        HUD = UI_Manager.instance;
    }

    
    void Update()
    {

        HUD.Health = Health;

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = camara.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0)); RaycastHit hit; if ((Physics.Raycast(ray, out hit) == true) && hit.distance < 5)
            {
                if (fuego.activeInHierarchy == true)
                {
                    if (hit.collider.name == "ARA�A")
                    {
                        Debug.Log("Le pegaste a la ara�a");
                        GameObject enemigo = GameObject.Find(hit.transform.name);
                        enemigo.GetComponent<ControlAra�a>().Asustarse();
                    }
                }
            }
        }

        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        if (Health <= 0)
        {
            Morir();
        }

        if (Input.GetKey(KeyCode.Escape))
        {
            Manager.AbandonarPartida();
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Auto"))
        {
            Morir();
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "InteriorCheck")
        {
            Manager.timerAndando = false;
            Debug.Log("Entro a la casa");
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "InteriorCheck")
        {
            Manager.timerAndando = true;
            Debug.Log("Salio de la casa");
        }
    }

    public void RecibirDa�o()
    {
        Health = Health - 1;
        Debug.Log(Health);
    }

    public void Morir()
    {
        Debug.Log("Te moriiiistessss");
        Manager.TerminarNoche();
    }
}
