using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;
    public Sonido[] sonidos;
    public string nowPlaying = "";
    GameManager Manager;
    float duracionStinger;
    bool stingerSonando;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);

        foreach (Sonido s in sonidos)
        {
            s.FuenteAudio = gameObject.AddComponent<AudioSource>();
            s.FuenteAudio.clip = s.Clip;
            s.FuenteAudio.volume = s.Volumen;
            s.FuenteAudio.pitch = s.pitch;
            s.FuenteAudio.loop = s.repetir;
        }
    }
    private void Start()
    {
        Manager = GameManager.instance;
    }

    private void Update()
    {
        Manager.stinger = stingerSonando;
        if(stingerSonando == true)
        {
            duracionStinger = duracionStinger - Time.deltaTime;
            if (duracionStinger <= 0)
            {
                stingerSonando = false;

            }
        }

    }

    public void Reproducir(string nombre)
    {
        Sonido s = Array.Find(sonidos, s => s.Nombre == nombre);
        if (s == null)
        {
            Debug.LogWarning("Sonido " + nombre + " no encontrado.");
            return;
        }
        else
        {
            s.FuenteAudio.Play();
            nowPlaying = nombre;
            if (nombre == "Stinger")
            {
                stingerSonando = true;
                duracionStinger = 23;
            }
        }
    }
    public void Pausar(string nombre)
    {
        Sonido s = Array.Find(sonidos, s => s.Nombre == nombre);
        if (s == null)
        {
            Debug.LogWarning("Sonido " + nombre + " no encontrado.");
            return;
        }
        else
        {
            s.FuenteAudio.Pause();
            nowPlaying = "";
        }
    }
}
