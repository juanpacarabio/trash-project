using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SillonScript : MonoBehaviour
{
    public Camera camara;
    GameManager Manager;
    void Start()
    {
        Manager = GameManager.instance;
    }

    void Update()
    {
        if (Input.GetKey("e"))
        {
            Ray ray = camara.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            RaycastHit hit;

            if ((Physics.Raycast(ray, out hit) == true) && hit.distance < 5)
            {
                if (hit.collider.name == "Sillon")
                {
                    Manager.Victoria();
                }
            }
        }   
    }
}
