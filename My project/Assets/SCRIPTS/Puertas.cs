using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puertas : MonoBehaviour
{
    public Camera camara;
        
 
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        

        if (Input.GetKeyDown(KeyCode.E))
        {
            Ray ray = camara.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            RaycastHit hit;
            if ((Physics.Raycast(ray, out hit) == true) && hit.distance < 5)
            {
                if (hit.collider.transform.parent.gameObject.CompareTag("Puerta") == true)
                {
                    GameObject test = hit.collider.transform.parent.gameObject;
                    Animator anim = test.GetComponent<Animator>();
                    if (anim.GetBool("Close") == true)
                    {
                        Puerta_Abrir(anim);

                    } 
                    else if (anim.GetBool("Open") == true)
                    {
                        Puerta_Cerrar(anim);
                    }
                }
                
            }
        }
    }

    private void Puerta_Abrir(Animator a)
    {
        Debug.Log("Estas tocando la puerta para abrir.");
        a.SetBool("Open", true);
        a.SetBool("Close", false);

    }
    private void Puerta_Cerrar(Animator a)
    {
        Debug.Log("Estas tocando la puerta para cerrar");
        a.SetBool("Open", false);
        a.SetBool("Close", true);
    }
   
}
