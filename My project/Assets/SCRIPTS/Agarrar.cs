using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Agarrar : MonoBehaviour
{
    public Camera camara;
    public GameObject[] objetos;
    public GameObject[] proyectiles;
    bool estaAgarrando = false;  // HAY QUE REFERENCIAR AL HUD (indica que se esta agarrando una bolsa)
    public GameObject encendedor;
    private UI_Manager HUD;
    void Start()
    {
        HUD = UI_Manager.instance;
    }

    void Update()
    {
        HUD.estaAgarrando = estaAgarrando;
        if (Input.GetKeyDown("e"))
        {
            if (estaAgarrando == false)
            {
                Ray ray = camara.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
                RaycastHit hit;

                if ((Physics.Raycast(ray, out hit) == true) && hit.distance < 5)
                {
                    Debug.Log(hit.collider.name);
                    if (hit.collider.tag == "Agarrable")    // 0 = bolsa chica, 1 = bolsa grande
                    {
                       
                        if (hit.collider.name.Substring(0, 8) == "BolsaB_G")
                        {
                            objetos[1].SetActive(true);
                            estaAgarrando = true;
                            hit.collider.enabled = false;
                            Destroy(hit.transform.gameObject);
                        }
                        else if (hit.collider.name.Substring(0, 8) == "BolsaB_M")
                        {
                            objetos[0].SetActive(true);
                            estaAgarrando = true;
                            hit.collider.enabled = false;
                            Destroy(hit.transform.gameObject);
                        }
                    }
                }
            }
            if (encendedor.activeInHierarchy == false)
            {
                Ray ray = camara.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
                RaycastHit hit;

                if ((Physics.Raycast(ray, out hit) == true) && hit.distance < 5)
                {
                    if (hit.collider.tag == "Encendedor")
                    {
                            encendedor.SetActive(true);
                            hit.collider.enabled = false;
                        GameObject encendedorEscenario = hit.collider.gameObject;
                        encendedorEscenario.SetActive(false);
                    }
                }
            }
        }
        if (Input.GetKeyDown("f"))
        {
            if (estaAgarrando == true)
            {
                
                if (objetos[1].activeInHierarchy == true)
                {
                    objetos[1].SetActive(false);
                    estaAgarrando = false;
                    Ray ray = camara.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

                    GameObject pro;
                    pro = Instantiate(proyectiles[1], ray.origin, transform.rotation);



                    Rigidbody rb = pro.GetComponent<Rigidbody>();
                    rb.AddForce(camara.transform.forward * 20, ForceMode.Impulse);
                }
                else if (objetos[0].activeInHierarchy == true)
                {
                    objetos[0].SetActive(false);
                    estaAgarrando = false;
                    Ray ray = camara.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

                    GameObject pro;
                    pro = Instantiate(proyectiles[0], ray.origin, transform.rotation);



                    Rigidbody rb = pro.GetComponent<Rigidbody>();
                    rb.AddForce(camara.transform.forward * 10, ForceMode.Impulse);
                }
            }
        }
    }
}
