using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharcoManager : MonoBehaviour
{
    public GameObject[] charcos;
    public GameManager Manager;
    void Start()
    {
        Manager = GameManager.instance;
    }

    void Update()
    {
        if (Manager.estaLloviendo == true)
        {
            foreach(GameObject charco in charcos)
            {
                charco.SetActive(true);
            }
        }
    }
}
