using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlEncendedor : MonoBehaviour
{
    public GameObject luz;
    public GameManager Manager;
    public GameObject encendedor;
    public float combustibleRestante; // HAY QUE REFERENCIAR AL HUD (Float que indica porcentaje restante del combustible)
    private UI_Manager HUD;
    public GameObject encendedor_escenario;
    bool puedeUsarse;
    void Start()
    {
        puedeUsarse = true;
        Manager = GameManager.instance;
        HUD = UI_Manager.instance;
        if(Manager.noche == 1)
        {
            combustibleRestante = 100;
        }else if (Manager.noche == 2)
        {
            combustibleRestante = 80;
        }else if (Manager.noche == 3)
        {
            combustibleRestante = 50;
        }
        else
        {
            combustibleRestante = 100;
        }
    }

    void Update()
    {
        HUD.combustibleRestante = combustibleRestante;
        HUD.encendedorPrendido = luz.activeInHierarchy;
        HUD.gasRestante.text = combustibleRestante.ToString("0");

        if (Input.GetKeyDown("q") && encendedor.activeInHierarchy == true)
        {
            encendedor.SetActive(false);
        }
        else if (Input.GetKeyDown("q") && encendedor.activeInHierarchy == false)
        {
            if (encendedor_escenario.activeInHierarchy == false)
            {
                encendedor.SetActive(true);
                if (puedeUsarse == false)
                {
                    luz.SetActive(false);
                }
            }
        }
        if (encendedor.activeInHierarchy == true)
        {
            if (Manager.estaLloviendo == true && Manager.timerAndando == true) // APAGA LA FUENTE DE LUZ CUANDO EL EVENTO DE LLUVIA SE ACTIVA EN EL GAMEMANAGER
            {
                luz.SetActive(false);
            }
            else if (Manager.estaLloviendo == false && puedeUsarse == true)
            {
                luz.SetActive(true);
            }
        }

        if (luz.activeInHierarchy == true)
        {
            combustibleRestante = combustibleRestante - Time.deltaTime;
        }
        if (combustibleRestante <= 0)
        {
            encendedor.SetActive(false);
            HUD.encendedorPrendido = false;
        }
    }
}
