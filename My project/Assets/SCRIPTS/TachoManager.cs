using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TachoManager : MonoBehaviour
{
    public GameObject[] tachos;
    public GameManager Manager;
    void Start()
    {
        Manager = GameManager.instance;
    }

    void Update()
    {
        if (Manager.cambioTachos == true)
        {
            if (tachos[0].activeInHierarchy == true)
            {
                int proximoTacho = Random.Range(0, 10);

                tachos[0].SetActive(false);
                if (proximoTacho < 5)
                {
                    tachos[1].SetActive(true);
                }else if (proximoTacho >= 5)
                {
                    tachos[2].SetActive(true);
                }
            }else if (tachos[1].activeInHierarchy == true)
            {
                int proximoTacho = Random.Range(0, 10);
                tachos[1].SetActive(false);
                if (proximoTacho < 5)
                {
                    tachos[0].SetActive(true);
                }else if (proximoTacho >= 5)
                {
                    tachos[2].SetActive(true);
                }
            }else if (tachos[2].activeInHierarchy == true)
            {
                int proximoTacho = Random.Range(0, 10);
                tachos[2].SetActive(false);
                if (proximoTacho < 5)
                {
                    tachos[0].SetActive(true);
                }
                else if (proximoTacho >= 5)
                {
                    tachos[1].SetActive(true);
                }
            }
            Manager.cambioTachos = false;
        }
    }
}
