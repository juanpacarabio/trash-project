using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public int bolsasTiradas; // HAY QUE REFERENCIAR AL HUD (Cantidad de bolsas ya tiradas)
    public int bolsasObjetivo;     // HAY QUE REFERENCIAR AL HUD (Cantidad de bolsas para completar noche)
    public static GameManager instance = null;
    public GameObject jugador;
    private UI_Manager HUD;
    GameObject sillon;
    public bool condicionCumplida;
    LevelManager Level;

    [Header("Control de Noches")]
    public int noche;
    [Header("Control de Eventos")]
    public bool pasarEvento;
    int evento;
    public bool estaLloviendo;
    public bool cambioTachos;
    public bool ara�aOut;
    public bool timerAndando;
    public bool alienON;
    public bool stinger;
    public GameObject auto;
    GameObject camion;
    public int intervalo = 5;
    
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
        
    }
    void Start()
    {
        HUD = UI_Manager.instance;
        pasarEvento = false;
        bolsasTiradas = 0;
        estaLloviendo = false;
        ara�aOut = false;
        jugador = GameObject.Find("Jugador");
        alienON = false;
        sillon = GameObject.Find("Sillon");
        condicionCumplida = false;
        Level = LevelManager.instance;
        noche = Level.noche;
        auto = GameObject.Find("AUTO Calle");
        auto.SetActive(false);
        camion = GameObject.Find("CAMION");
        camion.SetActive(false);
    }

    void Update()
    {
        intervalo = 5;
        HUD.bolsasTiradas = bolsasTiradas;
        HUD.bolsasObjetivo = bolsasObjetivo;

        if (pasarEvento == true)
        {
            evento = Random.Range(0, 6);
            if (evento == 0)
            {
                // lluvia
                if (estaLloviendo == false)
                {
                    Debug.Log("Evento Lluvia");
                    estaLloviendo = true;
                    AudioManager.instance.Reproducir("Tormenta");
                }
            }
            else if (evento == 1)
            {
                //Cambio de tachos
                Debug.Log("Evento Tachos");
                cambioTachos = true;
            }
            else if (evento == 2)
            {
                // Largar ara�a
                //Debug.Log("Evento Ara�a");
                //if (ara�aOut == false)
                //{
                //    ara�aOut = true;
                //}

            }
            else if (evento == 3)
            {
                // Alien
                Debug.Log("Evento Alien");
                if (alienON == false)
                {
                    alienON = true;
                }
            }
            else if (evento == 4)
            {
                //Debug.Log("Evento Stinger");
                if (stinger == false)
                {
                    AudioManager.instance.Reproducir("Stinger");
                }
            }
            else if (evento == 5)
            {
                Debug.Log("EventoAuto");
                auto.SetActive(true);
                Animator animAuto = auto.GetComponent<Animator>();
                animAuto.Play("Auto_Calle");
                
                
            }
            else if (evento == 6)
            {
                Debug.Log("Evento Cami�n");
                camion.SetActive(true);
                Animator animCamion = camion.GetComponent<Animator>();
                animCamion.Play("Camion_Calle");

            }
            else if (evento == 7)
            {
                Debug.Log("Pasar evento 7");
            }
            else if (evento == 8)
            {
                Debug.Log("Pasar evento 8");
            }
            else if (evento == 9)
            {
                Debug.Log("Pasar evento 9");
            }
            else if (evento == 10)
            {
                Debug.Log("Pasar evento 10");
            }
            pasarEvento = false;
        }

        if (bolsasTiradas >= bolsasObjetivo)
        {
            Outline outlineSillon = sillon.GetComponent<Outline>();
            outlineSillon.enabled = true;
            condicionCumplida = true;
        }
        
        
    }

    public void AbandonarPartida()
    {
        AudioManager.instance.Pausar("Tormenta");
        AudioManager.instance.Pausar("Stinger");
        Cursor.lockState = CursorLockMode.None;
        SceneManager.LoadScene("QUICK_MENU");
    }

    public void Victoria()
    {
        Debug.Log("Victoria!!!!");
        AudioManager.instance.Pausar("Tormenta");
        AudioManager.instance.Pausar("Stinger");
        HUD.PantallaVictoria();
    }
    public void TerminarNoche()
    {
        AudioManager.instance.Pausar("Tormenta");
        AudioManager.instance.Pausar("Stinger");
        HUD.PantallaGameOver();
    }
}
