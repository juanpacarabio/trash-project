using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SumarBolsas : MonoBehaviour
{
    public GameManager Manager;
    void Start()
    {
        Manager = GameManager.instance;
    }


    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name.Substring(0,6) == "BolsaB")
        {
            Debug.Log("Bolsa entro");
            Manager.bolsasTiradas = Manager.bolsasTiradas + 1;
            Destroy(collision.collider.gameObject);
        }
    }
}
