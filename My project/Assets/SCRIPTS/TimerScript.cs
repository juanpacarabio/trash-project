using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerScript : MonoBehaviour
{
    float tiempoMax;
    float tiempoActual;
    public int intervaloEventos;
    int tiempoTriggerEvento;
    public GameManager Manager;
    public bool estaAfuera;
    void Start()
    {
        intervaloEventos = 5;
        tiempoTriggerEvento = intervaloEventos;
        tiempoActual = 0;
        tiempoMax = 500;
    }


    void Update()
    {
        estaAfuera = Manager.timerAndando;
        if (estaAfuera == true)
        {
            if (tiempoActual <= tiempoMax)
            {
                tiempoActual += Time.deltaTime;
            }

            if (tiempoActual > tiempoTriggerEvento)
            {
                Evento();
                tiempoTriggerEvento += intervaloEventos;
            }
        }
    }

    void Evento()
    {
        Manager.pasarEvento = true;
    }
}
