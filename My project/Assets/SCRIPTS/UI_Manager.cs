using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UI_Manager : MonoBehaviour
{
    public static UI_Manager instance = null;
    GameObject gameManager;

    [Header("Im�genes")]
    public Image Vida_Full;
    public Image Vida_Med;
    public Image Vida_Low;
    public Image Bolsa_Hand;
    public Image Hand;
    public Image Lighter_On;
    public Image Lighter_Off;
    public Image Agarrar;
    public Image Icono_Contador;
    public TMPro.TMP_Text gasRestante;
    public TMPro.TMP_Text bolsasTot;
    public TMPro.TMP_Text bolsasRest;
    public GameObject gameOver;
    public GameObject victoria;



    [Header("Referencias")]
    public int Health;
    public bool estaAgarrando;
    public float combustibleRestante;
    public int bolsasTiradas;
    public int bolsasObjetivo;
    public bool encendedorPrendido;
    GameManager manager;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        } 
        else if(instance!=this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
   
    }


    void Start()
    {
        manager = GameManager.instance;
        gameManager = GameObject.Find("GameManager");
        bolsasTot.text = bolsasObjetivo.ToString();
        bolsasRest.text = bolsasTiradas.ToString();
    }

    void Update()
    {
        bolsasTot.text = bolsasObjetivo.ToString();
        bolsasRest.text = bolsasTiradas.ToString();

        if (Health == 3)
        {
            VidaMAX();
        }else if (Health == 2)
        {
            VidaMed();
        }else if (Health == 1)
        {
            VidaLow();
        }

        if (encendedorPrendido == true)
        {
            EncendedorPrendido();
        }else if (encendedorPrendido == false)
        {
            EncendedorAguado();
        }

        if (estaAgarrando == false)
        {
            ManoSola();
        }else if (estaAgarrando == true)
        {
            BolsaSola();
        }
    }
    private void VidaMAX()
    {
        Vida_Full.gameObject.SetActive(true);
        Vida_Low.gameObject.SetActive(false);
        Vida_Med.gameObject.SetActive(false);

    }
    private void VidaLow()
    {
        Vida_Full.gameObject.SetActive(false);
        Vida_Low.gameObject.SetActive(true);
        Vida_Med.gameObject.SetActive(false);

    }
    private void VidaMed()
    {
        Vida_Full.gameObject.SetActive(false);
        Vida_Low.gameObject.SetActive(false);
        Vida_Med.gameObject.SetActive(true);

    }
    private void EncendedorPrendido()
    {
        Lighter_On.gameObject.SetActive(true);
        Lighter_Off.gameObject.SetActive(false);
        gasRestante.gameObject.SetActive(true);

    }

    private void EncendedorAguado()
    {
        Lighter_On.gameObject.SetActive(false);
        Lighter_Off.gameObject.SetActive(true);

    }

    private void ManoSola()
    {
        Hand.gameObject.SetActive(true);
        Bolsa_Hand.gameObject.SetActive(false);
    }
    private void BolsaSola()
    {
        Hand.gameObject.SetActive(false);
        Bolsa_Hand.gameObject.SetActive(true);

    }

    public void PantallaVictoria()
    {
        StartCoroutine(Victoria());
    }
    public IEnumerator Victoria()
    {
        victoria.SetActive(true);
        yield return new WaitForSeconds(5f);
        SceneManager.LoadScene("QUICK_MENU");
        yield return new WaitForSeconds(1f);
        Cursor.lockState = CursorLockMode.None;
    }

    public void PantallaGameOver()
    {
        StartCoroutine(GameOver());
    }
    public IEnumerator GameOver()
    {
        gameOver.SetActive(true);
        yield return new WaitForSeconds(5f);
        SceneManager.LoadScene("QUICK_MENU");
        yield return new WaitForSeconds(1f);
        Cursor.lockState = CursorLockMode.None;
    }

}
