using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienManager : MonoBehaviour
{
    public GameObject sensor;
    public GameObject alien;
    public GameManager Manager;
    public bool jugadorCerca;
    void Start()
    {
        Manager = GameManager.instance;
    }

    void Update()
    {
        jugadorCerca = sensor.GetComponent<SensorAlien>().jugadorCerca;
        if (jugadorCerca == true)
        {
            Manager.alienON = false;
        }
        if (Manager.alienON == false)
        {
            alien.SetActive(false);
            Manager.alienON = false;
        }
        if (Manager.alienON == true)
        {
            alien.SetActive(true);
        }
    }
}
