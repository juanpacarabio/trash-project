using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArañaManager : MonoBehaviour
{
    private GameManager Manager;
    public GameObject araña;
    void Start()
    {
        Manager = GameManager.instance;
    }

    void Update()
    {
        araña.SetActive(Manager.arañaOut);
    }
}
