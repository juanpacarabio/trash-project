using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ElegirNocheQ : MonoBehaviour
{
    LevelManager Manager;
    GameObject gameManager;
    public GameObject pantalla3;
    public GameObject pantallaTeclas;
    public GameObject creditos;
    public GameObject N1;
    public GameObject N2;
    public GameObject N3;
    public SceneManager D1;
    void Start()
    {
        Manager = LevelManager.instance;
        gameManager = GameObject.Find("GameManager");
    }

    public void ComenzarNoche1()
    {
        pantalla3.SetActive(false);
        StartCoroutine(Intro1());
    }

    public void ComenzarNoche2()
    {
        pantalla3.SetActive(false);
        StartCoroutine(Intro2());

    }

    public void ComenzarNoche3()
    {
        pantalla3.SetActive(false);
        StartCoroutine(Intro3());
    }

    public void ComenzarPartida()
    {
        pantallaTeclas.SetActive(false);
        creditos.SetActive(false);
        pantalla3.SetActive(true);
    }

    public void Cr�ditos()
    {
        creditos.SetActive(true);
        pantalla3.SetActive(false);
        pantallaTeclas.SetActive(false);
    }

    public void Teclas()
    {
        pantalla3.SetActive(false);
        pantallaTeclas.SetActive(true);
    }

    public void Atras()
    {
        SceneManager.LoadScene("MENU");
    }

    public void Salir()
    {
        Application.Quit();
    }

    IEnumerator Intro1()
    {
        N1.SetActive(true);
        yield return new WaitForSeconds(4.5f);
        SceneManager.LoadScene("TEST_Scene 1");
        Manager.noche = 1;
        gameManager.SetActive(true);
    }
    IEnumerator Intro2()
    {
        N2.SetActive(true);
        yield return new WaitForSeconds(4.5f);
        SceneManager.LoadScene("Noche2");
        Manager.noche = 2;
        gameManager.SetActive(true);
    }
    IEnumerator Intro3()
    {
        N3.SetActive(true);
        yield return new WaitForSeconds(4.5f);
        SceneManager.LoadScene("Noche3");
        Manager.noche = 3;
        gameManager.SetActive(true);
    }


}
