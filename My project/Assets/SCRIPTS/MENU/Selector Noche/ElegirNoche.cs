using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ElegirNoche : MonoBehaviour
{
    LevelManager Manager;
    public GameObject pantalla2;
    public GameObject pantalla3;
    public GameObject pantallaTeclas;
    public GameObject creditos;
    public GameObject N1;
    public GameObject N2;
    public GameObject N3;
    public SceneManager D1;
    void Start()
    {
        Manager = LevelManager.instance;
    }

    public void ComenzarNoche1()
    {
        pantalla3.SetActive(false);
        pantalla2.SetActive(false);
        StartCoroutine(Intro1());
    }

    public void ComenzarNoche2()
    {
        pantalla3.SetActive(false);
        pantalla2.SetActive(false);
        StartCoroutine(Intro2());

    }

    public void ComenzarNoche3()
    {
        pantalla3.SetActive(false);
        pantalla2.SetActive(false);
        StartCoroutine(Intro3());
    }

    public void ComenzarPartida()
    {
        pantalla2.SetActive(false);
        pantalla3.SetActive(true);
        pantallaTeclas.SetActive(false);
    }

    public void Cr�ditos()
    {
        creditos.SetActive(true);
        pantalla2.SetActive(false);
        pantalla3.SetActive(false);
        pantallaTeclas.SetActive(false);
    }

    public void Teclas()
    {
        pantalla2.SetActive(false);
        pantalla3.SetActive(false);
        pantallaTeclas.SetActive(true);
    }

    public void Atras()
    {
        SceneManager.LoadScene("MENU");
    }

    public void Salir()
    {
        Application.Quit();
    }

    IEnumerator Intro1()
    {
        N1.SetActive(true);
        yield return new WaitForSeconds(4.5f);
        // Manager.noche = 1;
        SceneManager.LoadScene("TEST_Scene 1");
    }
    IEnumerator Intro2()
    {
        N2.SetActive(true);
        yield return new WaitForSeconds(4.5f);
        SceneManager.LoadScene("Noche2");
    }
    IEnumerator Intro3()
    {
        N3.SetActive(true);
        yield return new WaitForSeconds(4.5f);
        SceneManager.LoadScene("Noche3");
    }


}
