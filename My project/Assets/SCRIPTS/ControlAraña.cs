using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlAraña : MonoBehaviour
{
    private GameObject jugador;
    public GameObject[] posiciones;
    public int rapidez;
    public bool escapando;
    public float cooldownAtaque;
    public GameObject araña;
    private GameManager Manager;
    bool  objetivoDecidido = false;
    int objetivo;
    void Start()
    {
        jugador = GameObject.Find("Jugador");
        escapando = false;
        Manager = GameManager.instance;
    }

    
    void Update()
    {
        cooldownAtaque = cooldownAtaque - Time.deltaTime;
        if (escapando == false)
        {
            transform.LookAt(jugador.transform);
            transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
        }else if (escapando == true)
        {
            if(objetivoDecidido == false)
            {
                objetivo = Random.Range(0, 3);
                objetivoDecidido = true;
            }
            transform.LookAt(posiciones[objetivo].transform);
            transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
        }else if (araña.activeInHierarchy == false)
        {
            transform.LookAt(posiciones[Random.Range(0, 3)].transform);
            transform.Translate(rapidez * Vector3.forward * Time.deltaTime);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (cooldownAtaque <= 0)
        {
            if (collision.gameObject.name == "Jugador")
            {
                jugador.GetComponent<ControlJugador>().RecibirDaño();
            }else if (collision.gameObject.tag == "PuntoAraña")
            {
                araña.SetActive(false);
                Manager.arañaOut = false;
                objetivoDecidido = false;
                escapando = false;
            }
        }
    }

    public void Asustarse()
    {
        escapando = true;
    }

}
