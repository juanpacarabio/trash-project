using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienMirar : MonoBehaviour
{
    GameObject jugador;
    void Start()
    {
        jugador = GameObject.Find("Jugador");
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(jugador.transform);
    }
}
