using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Encendedor : MonoBehaviour
{
    Animator EN;
    public GameObject Lighter;
    // Start is called before the first frame update
    void Start()
    {
        EN = this.gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("Vertical") || Input.GetKey("Horizontal"))
        {
            //se esta moviendo
            if (EN.GetBool("TapaON") == true) // Se esta moviendo con la tapa puesta.
            {
                CaminandoON();
            }
            if (EN.GetBool("TapaOFF") == true) // Se esta moviendo con la tapa cerrada.
            {
                CaminandoOFF();
            }
        } else if (Input.GetKeyUp("Vertical") || Input.GetKey("Horizontal"))

        {
            if (EN.GetBool("TapaON") == true) // Dejo de moverse con la tapa puesta.
            {
                Idle_ON();
            }
            if (EN.GetBool("TapaOFF") == true) // Dejo de moverse con la tapa cerrada.
            {
                Idle_OFF();

            }
        }

        if (Input.GetKeyDown(KeyCode.Q)) // Esta guardando o sacando el encendedor.
        {
            if (Lighter.activeInHierarchy == true)
            {
                Salir();
                Lighter.SetActive(false);

            }
            if (Lighter.activeInHierarchy == false)
            {
                Lighter.SetActive(true);

            }
        }
        if (Input.GetKeyDown(KeyCode.F)) // Esta prendiendo o apagando el encendedor.
        {
            if (EN.GetBool("TapaON") == true)
            {
                OFF();
            }
            if (EN.GetBool("TapaOFF") == true)
            {
                ON();
            }
        }
    }

    private void CaminandoON()
    {
        EN.SetBool("EstaCaminando", true);
        EN.SetBool("TapaOFF", false);

    }
    private void CaminandoOFF()
    {
        EN.SetBool("EstaCaminando", true);
        EN.SetBool("TapaON", false);

    }
    private void OFF()
    {
        EN.SetBool("TapaON", false);
        EN.SetBool("TapaOFF", true);
        EN.SetBool("CambioOFF", true);
        EN.SetBool("CambioON", false);


    }
    private void ON()
    {
        EN.SetBool("TapaOFF", false);
        EN.SetBool("TapaON", true);
        EN.SetBool("CambioOFF", false);
        EN.SetBool("CambioON", true);



    }

    private void Salir()
    {
        EN.SetBool("Salir", true);

    }

    private void Idle_ON()
    {
        EN.SetBool("EstaCaminando", false);
        EN.SetBool("TapaOFF", false);

    }
    private void Idle_OFF()
    {
        EN.SetBool("EstaCaminando", false);
        EN.SetBool("TapaON", false);

    }


}
