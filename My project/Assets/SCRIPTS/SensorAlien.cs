using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SensorAlien : MonoBehaviour
{
    public bool jugadorCerca;
    void Start()
    {
        jugadorCerca = false;
    }

    void Update()
    {
        
    }

    //private void OnCollisionEnter(Collision collision)
    //{
    //    if (collision.gameObject.name == "Jugador")
    //    {
    //        jugadorCerca = true;
    //    }
    //}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Jugador")
        {
            jugadorCerca = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "Jugador")
        {
            jugadorCerca = false;
        }
    }
}
