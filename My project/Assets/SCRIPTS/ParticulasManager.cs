using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticulasManager : MonoBehaviour
{
    public GameObject lluvia;
    public GameManager Manager;
    void Start()
    {
        Manager = GameManager.instance;
    }

    void Update()
    {
        if (Manager.estaLloviendo == true)
        {
            if (Manager.timerAndando == true)
            {
                lluvia.SetActive(true);
            }
        }else if (Manager.estaLloviendo == false)
        {
            lluvia.SetActive(false);
        }
    }
}
