using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Realentizador : MonoBehaviour
{
    public GameObject jugador;
    void Start()
    {
        jugador = GameObject.Find("Jugador");
    }

    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject == jugador)
        {
            jugador.GetComponent<ControlJugador>().rapidezDesplazamiento = jugador.GetComponent<ControlJugador>().rapidezDesplazamiento / 2;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject == jugador)
        {
            jugador.GetComponent<ControlJugador>().rapidezDesplazamiento = jugador.GetComponent<ControlJugador>().rapidezDesplazamiento * 2;
        }
    }
}
